package org.beetl.sql.test;
import java.math.*;
import java.sql.*;
/*
* 
* gen by beetsql 2015-12-11
*/
public class User  {
	private Integer roleId ;
	private String name ;
	private Integer id ;
	private String userName ;
	private Integer age ;
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}

}
